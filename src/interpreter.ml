let readFile file_name =
  let channel = open_in file_name in
  let rec read () =
    try
      let text = input_line channel in text ^ read ()
    with End_of_file -> close_in channel; ""
  in
    Tokenizer.tokenize (read ()) ;;

let printTokenType = function
  | Tokenizer.Identificator -> print_string "Identificator : "
  | Tokenizer.Punctuator -> print_string "Punctuator : "
  | Tokenizer.ReservedWord -> print_string "ReservedWord : " ;;

let printToken (app_token : Tokenizer.token) =
  let open Tokenizer in
  let () = printTokenType app_token.type_ in
  print_endline app_token.str_ ;;

let rec showToken (app_tokens : Tokenizer.tokens) = match app_tokens with
  | [] -> ()
  | current_token :: other_tokens ->
      let () = printToken current_token in
      showToken other_tokens ;;

let showTokens file_name =
  showToken (readFile file_name) ;;

Arg.parse [] showTokens "Usage: lamba <input files>\n";
