type token_type =
  | Identificator
  | Punctuator
  | ReservedWord

type position = int * int

type token = {
  str_ : string;
  type_ : token_type;
  pos_ : position;
}

type tokens = token list

val tokenize : string -> tokens
