type token_type =
  | Identificator
  | Punctuator
  | ReservedWord

type position = int * int

type token = {
  str_ : string;
  type_ : token_type;
  pos_ : position;
}

type tokens = token list

type state = {
  mutable token_ : string;
  mutable tokens_ : tokens;
  mutable position_ : position;
}

let incrementPosition (pos : position) =
  (fst pos, snd pos + 1) ;;

let incrementLine (pos : position) =
  (fst pos + 1, snd pos) ;;

let isPunctuator = function
  | "(" | ")" | "\\" | "." | "," | "|" | ":" | ";" -> true
  | _ -> false ;;

let isCharPunctuator (ch : char) =
  isPunctuator (Utils.string_of_char ch) ;;

let isReserved = function
  | "in" | "is" | "let" | "type" -> true
  | _ -> false ;;

let isSpace = function
  | '\n' | '\t' | ' ' | '\r' -> true
  | _ -> false ;;

let isNewline = function
  | '\n' | '\r' -> true
  | _ -> false ;;

let checkNewLine (ch : char) (current_state : state) =
  if (isNewline ch) then
    current_state.position_ <- incrementLine current_state.position_ ;;

let getTokenType (current_token : string) =
  if (isReserved current_token) then
    ReservedWord
  else if (isPunctuator current_token) then
    Punctuator
  else
    Identificator ;;

let addToken (current_state : state) =
  if (not (Utils.isEmpty current_state.token_)) then
    let new_token = {
      str_ = current_state.token_;
      type_ = getTokenType current_state.token_;
      pos_ = current_state.position_;
    } in
    current_state.token_ <- "";
    current_state.tokens_ <- new_token :: current_state.tokens_ ;;

let processChar (ch : char) (current_state : state) =
  let () = current_state.position_ <-
    incrementPosition current_state.position_ in
  if (not (isSpace ch)) then
    let () = if (isPunctuator current_state.token_ || isCharPunctuator ch) then
      addToken current_state
    in current_state.token_ <- current_state.token_ ^ (Utils.string_of_char ch)
  else
    let () = checkNewLine ch current_state in
    addToken current_state ;;

let tokenize (str : string) =
  let current_state = {
    token_ = "";
    tokens_ = [];
    position_ = (0, 0);
  } in
  let process (ch : char) = processChar ch current_state in
  let () = String.iter process str in
  let () = addToken current_state in
  List.rev current_state.tokens_ ;;
