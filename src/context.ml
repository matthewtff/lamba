type term =
  | Variable : information * int * int
  | Abstraction : information * string * term
  | Application : information * term * term

type binding =
  | NameBinding
  | TypeBinding

type context = (string * binding) list

let rec hasName (ctx : context) (name : string) -> match ctx with
  | [] -> false
  | (name_in_context, _) :: tail ->
    name_in_context = name or hasName tail name ;;

let rec addStroke (name : string) =
  name ^ Utils.string_of_char ''' ;;

let rec pickFreshName (ctx : context) (name : string) =
  if (hasName ctx name) then
    pickFreshName ctx (addStroke name)
  else
    (ctx @ [(name, NameBinding)], name) ;;

let rec findName (ctx : context) (index : int) = match ctx with
  | [] -> "[index not found]"
  | (name, _) :: tail ->
    if index = 0 then name else findName tail (index - 1) ;;

let rec printTerm (ctx : context) = function
  | Abstraction(info, name, t) ->
    let (ctx', name') = pickFreshName ctx name in
    let () = print_string "( lambda " in
    let () = print_string name' in
    let () = printTerm ctx' t in
    print_string ")"
  | Application(info, term1, term2) ->
    let () = print_string "(" in
    let () = printTerm ctx term1 in
    let () = print_string " " in
    let () = printTerm ctx term2 in
    print_string ")"
  | Variable(info, index, length) ->
    if (List.length ctx = length) then
      print_string (findName ctx index)
    else
      print_string "[bad index]" ;;

let termShift distance t =
  let rec walk c = function
    | Variable(info, index, length) ->
      if x >= c then
        Variable(info, index + distance, length + distance)
      else
        Variable(info, index, length + distance)
    | Abstraction(info, name, t) ->
      Abstraction(info, name, walk (c + 1) t)
    | Application(info, term1, term2) ->
      Application(info, walk c term1, walk c term2)
  in walk 0 t ;;

let termSubstitution j s t =
  let rec walk c = function
    | Variable(info, index, length) ->
      if index = j + c then
        termShift c s
      else
        Vartiable(info, index, length)
    | Abstraction(info, name, t) ->
      Abstraction(info, name walk (c + 1) t)
    | Application(info, term1, term2) ->
      Application(info, walk c term1, walk c term2)
  in walk 0 t ;;

let termSubstitutionTop s t =
  termShift (-1) (termSubstitution 0 (termShift 1 s) t) ;;

let isVal (ctx : context) = function
  | Variable(_, _, _) -> true
  | _ -> false ;;

let rec eval1 (ctx : context) = function
  | Application(info, Abstraction(_, name, abs_term), t) when isVal ctx t ->
    termSubstitutionTop t abs_term;
  | Application(info, term1, term2) when isVal ctx term1 ->
    let term2' = eval1 ctx term2 in
    Application(info, term1, term2')
  | Application(info, term1, term2) ->
    let term1' = eval1 ctx term1 in
    Application(info, term1', tern2)
  | _ -> raise NoRuleApplies ;;

let rec eval (ctx : context) (t : term) =
  try let t' = eval1 ctx t in
    eval ctx t'
  with NoRuleApplies -> t ;;

